<?php

namespace Drupal\user_plugin\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\ViewExecutable;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Component\Render\FormattableMarkup;

/**
 * A handler to provide a Confirm User delete
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("confirm_user_del_field")
 */
class ConfirmUserDelete extends FieldPluginBase {

  /**
   * The current display.
   *
   * @var string
   *   The current display of the view.
   */
  protected $currentDisplay;

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);
    $this->currentDisplay = $view->current_display;
  }

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing -- to override the parent query.
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    // First check whether the field should be hidden if the value(hide_alter_empty = TRUE) /the rewrite is empty (hide_alter_empty = FALSE).
    $options['hide_alter_empty'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $user = $values->_entity;
    $uid = $user->id();
    $encoded_url = base64_encode(\Drupal::service('path.current')->getPath());
    $url = Url::fromRoute('user_plugin.custom_user_delete', array('uid' => $uid, 'redirect' => $encoded_url));
    $link_options = [
          'attributes' => [
            'class' => [
              'confirm-ajax-delete',
              'use-ajax',
            ],
            'data-dialog-type' => 'modal',
            'data-dialog-options' => json_encode([
              'width' => 300,
              'height' => 200
            ]),
          ],
        ];
    $url->setOptions($link_options);
    //$project_link = \Drupal::l(t('Delete'), $url);
    
    $project_link = array(
      '#type' => 'link',
      '#prefix' => '<span class="delete-icon">',
      '#title' => t(''),
      '#url' => $url,
      '#suffix' => '</span>',
      '#attached' => [
        'library' => [
          'core/drupal.dialog.ajax',
        ]
      ],
    );
    //$project_link = Link::fromTextAndUrl(t('Delete'), $url);

    return $project_link;
  }

}
