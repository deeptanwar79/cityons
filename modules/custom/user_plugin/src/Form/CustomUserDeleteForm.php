<?php

namespace Drupal\user_plugin\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Ajax\CloseModalDialogCommand;

/**
 * Defines a confirmation form to confirm deletion of something by id.
 */
class CustomUserDeleteForm extends FormBase {

  /**
   * ID of the item to delete.
   *
   * @var int
   */
  protected $uid;
  protected $redirect;

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return "confirm_delete_form";
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, string $uid = NULL, string $redirect = NULL) {
    $this->uid = $uid;
    $this->redirect = $redirect;
    $form['markup'] = [
      '#type' => 'item',
      '#field_prefix' => $this->t('Are you sure you want to delete this User?'),
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#name' => 'submit',
      '#value' => $this->t('Confirm'),
      '#button_type' => 'primary',
    );
    $form['actions']['cancel_button'] = [
      '#id' => 'cancel-button',
      '#type' => 'submit',
      '#value' => $this->t('Cancel'),
      '#ajax' => [
        'callback' => '::cancelButtonCallback',
      ],
      '#weight' => '99',
    ];
    return $form;
  }


  public function cancelButtonCallback(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $response->addCommand(new CloseModalDialogCommand());
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $buttonName = $form_state->getTriggeringElement()['#name'];
    if ($buttonName == 'submit') {
      $redirect_url = base64_decode($this->redirect);
      $url = Url::fromUri('internal:' . $redirect_url);
      // To delete User from list
      if (\Drupal::currentUser()->id() != $this->uid) {
        $user = \Drupal\user\Entity\User::load($this->uid);
        $msg = 'User: ' . $this->uid . ' is deleted';
        \Drupal::messenger()->addStatus($msg);
        $user->delete();
        
        return $form_state->setRedirectUrl($url);
      } else {
        \Drupal::messenger()->addStatus('Logged in user cannot delete its own account.');
        return $form_state->setRedirectUrl($url);
      }
      
    }
  }
}
