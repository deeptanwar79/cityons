<?php

namespace Drupal\cityons_core\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Ajax\CloseModalDialogCommand;

/**
 * Defines a confirmation form to confirm deletion of something by id.
 */
class CustomUserUpdateForm extends FormBase {

    /**
   * ID of the item to delete.
   *
   * @var int
   */
  protected $uid;

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return "user_update_form";
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, string $uid = NULL) {
    $this->uid = $uid;
    $form['#theme'] = 'change_information_form';
    $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
    $fname = $user->field_first_name->value;
    $lname = $user->field_last_name->value;
    $phone = $user->field_phone_number->value;
    $cell = $user->field_cell_phone->value;
    $form['last_name'] = array(
        '#type' => 'textfield',
        '#title' => t('Last Name:'),
        '#required' => TRUE,
        '#default_value' => $lname,
    );

    $form['first_name'] = array(
        '#type' => 'textfield',
        '#title' => t('First Name:'),
        '#required' => TRUE,
        '#default_value' => $fname,
    );

    $form['phone_number'] = array (
        '#type' => 'tel',
        '#title' => t('Phone Number'),
        '#required' => TRUE,
        '#default_value' => $phone,
      );
    
    $form['cell_phone'] = array (
    '#type' => 'tel',
    '#title' => t('Cell Phone'),
    '#default_value' => $cell,
    );  

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
      '#attributes' => array(
        'class' => ['btn btn-primary waves-effect waves-light']
      ),
    );
    $reset_btn = array(
        '#type' => 'button',
        '#button_type' => 'reset',
        '#value' => t('Reset'),
        '#weight' => 20,
        '#validate' => array(),
        '#attributes' => array(
          'class' => ['btn btn-light waves-effect'],
          'onclick' => 'this.form.reset(); return false;',
        ),
      );
    $form['actions']['resetbutton'] = $reset_btn;   
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!is_numeric($form_state->getValue('phone_number'))) {
        $form_state->setErrorByName('phone_number', $this->t('Phone number should contain numbers only.'));
      }

      if (!is_numeric($form_state->getValue('cell_phone')) && !empty($form_state->getValue('cell_phone'))) {
        $form_state->setErrorByName('cell_phone', $this->t('Cell number should contain numbers only.'));
      }  
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();  
    $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
    $user->set('field_first_name', $values['first_name']);
    $user->set('field_last_name', $values['last_name']);
    $user->set('field_phone_number', $values['phone_number']);
    $user->set('field_cell_phone', $values['cell_phone']);
    
    $user->save();
  }
}