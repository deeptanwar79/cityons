<?php

use Drupal\user\Plugin\Validation\Constraint\UserNameUnique;

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * {@inheritdoc}
 */
function remove_username_form_user_register_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state) {
  $form['account']['name']['#required'] = FALSE;
  $form['account']['name']['#access'] = FALSE;
  $form['account']['mail']['#required'] = TRUE;
  array_unshift($form['#validate'], 'remove_username_prepare_form_user_values');
  $form['#validate'][] = 'remove_username_form_user_post_validate';
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * {@inheritdoc}
 */
function remove_username_form_user_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state) {
  $form['account']['name']['#required'] = FALSE;
  $form['account']['name']['#access'] = FALSE;
  $form['account']['mail']['#required'] = TRUE;
  array_unshift($form['#validate'], 'remove_username_prepare_form_user_values');
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * {@inheritdoc}
 */
function remove_username_form_user_login_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state) {
  $form['name']['#title'] = \Drupal::translation()->translate('Email address');
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * {@inheritdoc}
 */
function remove_username_form_user_pass_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state) {
  $form['name']['#title'] = \Drupal::translation()->translate('Email address');
}

/**
 * Copy the 'mail' field to the 'name' field before form validation.
 *
 * @param array $form
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 */
function remove_username_prepare_form_user_values(&$form, \Drupal\Core\Form\FormStateInterface $form_state) {
  $email = $form_state->getValue('mail');

  if ($user = user_load_by_name($email)) {
    /** @var \Drupal\Core\Entity\EntityFormInterface $formObject */
    $formObject = $form_state->getFormObject();

    if ($user->id() != $formObject->getEntity()->id()) {
      $constraint = new UserNameUnique();
      $form_state->setErrorByName('mail', t($constraint->message, ['%value' => $email]));
    }
  }

  if ($error = user_validate_name($email)) {
    $form_state->setErrorByName('mail', $error);
  }

  $form_state->setValue('name', $email);
}

/**
 * Remove the errors related to 'name' field.
 * @param $form
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 */
function remove_username_form_user_post_validate(&$form, \Drupal\Core\Form\FormStateInterface $form_state) {
  $errors = $form_state->getErrors();
  unset($errors['name']);
  $form_state->clearErrors();
  foreach($errors as $field => $value) {
    $form_state->setErrorByName($field, $value);
  }
}
