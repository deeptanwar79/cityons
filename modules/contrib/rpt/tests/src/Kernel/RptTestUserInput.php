<?php

namespace Drupal\Tests\rpt\Kernel;

use Drupal\Core\Form\FormState;
use Drupal\KernelTests\KernelTestBase;

/**
 * Test RPT fully.
 *
 * @group rpt
 */
class RptTestUserInput extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'rpt',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->installEntitySchema('user');
    \Drupal::service('router.builder')->rebuild();

    /** @var \Drupal\Core\Config\ConfigFactoryInterface $configFactory */
    $configFactory = \Drupal::service('config.factory');
    $configFactory->getEditable('user.settings')
      ->set('verify_mail', FALSE)
      ->save();
    $configFactory->getEditable('rpt.settings')
      ->set('password_generate', FALSE)
      ->save();
  }

  /**
   * Test predefined passwords.
   *
   * @dataProvider getUserInputPasswordData
   */
  public function testUserInputPassword($pass) {
    /** @var \Drupal\Core\Form\FormBuilderInterface $formBuilder */
    $formBuilder = $this->container->get('form_builder');
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager */
    $entityTypeManager = $this->container->get('entity_type.manager');
    /** @var \Drupal\Core\Utility\Token $entityTypeManager */
    $token = $this->container->get('token');

    $entity = $entityTypeManager->getStorage('user')->create([]);
    $formObject = $entityTypeManager->getFormObject('user', 'register')
      ->setEntity($entity);
    $formState = new FormState();
    $formObject->buildForm([], $formState);
    $formState->setValues([
      'mail' => 'test@example.com',
      'name' => 'test',
      'pass' => [
        'pass1' => $pass,
        'pass2' => $pass,
      ],
    ]);
    $formBuilder->submitForm($formObject, $formState);

    $replacedToken = $token->replace('[user:password]', [
      'user' => $formState->getFormObject()->getEntity(),
    ]);
    $this->assertSame($pass, $replacedToken);

    $replacedToken = $token->replace('[account:password]', [
      'user' => $formState->getFormObject()->getEntity(),
    ]);
    $this->assertSame($pass, $replacedToken);
  }

  /**
   * Provide data for testUserInputPassword().
   *
   * @return array
   *   Dummy passwords.
   */
  public function getUserInputPasswordData() {
    return [
      ['fM4izYvqTS'],
      ['CposTronfg'],
      ['cGZ2oNAAut'],
      ['3gbi2ztqfC'],
      ['PeaTAwNreX'],
    ];
  }

}
