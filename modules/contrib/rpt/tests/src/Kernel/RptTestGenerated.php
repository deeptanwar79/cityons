<?php

namespace Drupal\Tests\rpt\Kernel;

use Drupal\Core\Form\FormState;
use Drupal\KernelTests\KernelTestBase;

/**
 * Test RPT fully.
 *
 * @group rpt
 */
class RptTestGenerated extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'rpt',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->installEntitySchema('user');
    \Drupal::service('router.builder')->rebuild();

    /** @var \Drupal\Core\Config\ConfigFactoryInterface $configFactory */
    $configFactory = \Drupal::service('config.factory');
    $configFactory->getEditable('user.settings')
      ->set('verify_mail', FALSE)
      ->save();
    $configFactory->getEditable('rpt.settings')
      ->set('password_generate', TRUE)
      ->save();
  }

  /**
   * Test generated passwords.
   *
   * @dataProvider getGeneratedPasswordData
   */
  public function testGeneratedPassword($length) {
    $configFactory = \Drupal::service('config.factory');
    $configFactory->getEditable('rpt.settings')
      ->set('password_length', $length)
      ->save();

    /** @var \Drupal\Core\Form\FormBuilderInterface $formBuilder */
    $formBuilder = $this->container->get('form_builder');
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager */
    $entityTypeManager = $this->container->get('entity_type.manager');
    /** @var \Drupal\Core\Utility\Token $entityTypeManager */
    $token = $this->container->get('token');

    $entity = $entityTypeManager->getStorage('user')->create([]);
    $formObject = $entityTypeManager->getFormObject('user', 'register')
      ->setEntity($entity);
    $formState = new FormState();
    $formObject->buildForm([], $formState);
    $formState->setValues([
      'mail' => 'test@example.com',
      'name' => 'test',
    ]);
    $formBuilder->submitForm($formObject, $formState);

    $replacedToken = $token->replace('[user:password]', [
      'user' => $formState->getFormObject()->getEntity(),
    ]);
    $this->assertSame($length, mb_strlen($replacedToken));
  }

  /**
   * Provide data for testUserInputPassword().
   *
   * @return array
   *   Lengths of dummy password to test.
   */
  public function getGeneratedPasswordData() {
    return [
      [10],
      [20],
      [15],
      [35],
      [1],
    ];
  }

}
