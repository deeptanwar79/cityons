/* Load jQuery.
--------------------------*/
jQuery(document).ready(function ($) {
  // Mobile menu.
  $('.mobile-menu').click(function () {
    $(this).next('.primary-menu-wrapper').toggleClass('active-menu');
  });
  $('.close-mobile-menu').click(function () {
    $(this).closest('.primary-menu-wrapper').toggleClass('active-menu');
  });
  // Full page search.
  $('.search-icon').on('click', function() {
    $('.search-box').addClass('open');
    return false;
  });

  $('.search-box-close').on('click', function() {
    $('.search-box').removeClass('open');
    return false;
  });

  // Scroll To Top.
  $(window).scroll(function () {
    if ($(this).scrollTop() > 80) {
      $('.scrolltop').fadeIn('slow');
    } else {
      $('.scrolltop').fadeOut('slow');
    }
  });
  $('.scrolltop').click(function () {
    $('html, body').animate( { scrollTop: 0 }, 'slow');
  });

  // Wrap homepage blocks
  $(".region-content-home .block").wrapInner( '<div class="container"></div>' );
  $(".region-content-home .block:nth-child(even)").prepend( '<div class="home-block-cicle1"></div><div class="home-block-cicle2"></div><div class="home-block-cicle3"></div>' );
/* End document
--------------------------*/

  $('#edit-field-agency').hide();
  $('.user-register-form p.district-markup').hide();
  $('.user-register-form .field--name-field-districts').hide();
  $("#edit-field-roles").change(function(){
    if ( $(this).val() == 'school_admin' || $(this).val() == 'esc_admin') {
      $('#edit-field-agency').show();
      $('.user-register-form p.district-markup').show();
      $('.user-register-form .field--name-field-districts').show();
    } else if($(this).val() == 'region_18_admin') {
      $('#edit-field-agency').hide();
      $('.user-register-form p.district-markup').show();
      $('.user-register-form .field--name-field-districts').show();
    } else {
      $('#edit-field-agency').hide();
      $('.user-register-form p.district-markup').hide();
      $('.user-register-form .field--name-field-districts').hide();
    }
  });

  // Reset Button
  $('.user-register-form input.button--reset').on('click', function() {
    $('.field--name-field-districts input.form-autocomplete').val('');
  });

  $(document).ajaxComplete(function() {
    jQuery("body").find('.form-autocomplete').change(function() {console.log('tets');
    var val = jQuery(this).val();
    var match = val.match(/\((.*?)\)$/);
    if (match) {
        jQuery(this).data('real-value', val);
        jQuery(this).val(val.replace(' ' + match[0], ''));
    }
});
  });
  
});