jQuery(".menu-tgl").click(function(){
    jQuery(this).toggleClass("act");	
    jQuery(".main-wrapper").toggleClass("left-colapse");
    jQuery(".custom-3wdt").toggleClass("custom-colapse");
    });

    
    jQuery(".menu-tgl").click(function(){
      jQuery(".top-noti-user").toggleClass("act-topnt");		
      });
    
    jQuery('.delete-icon .confirm-ajax-delete').html('<i class="fas fa-trash-alt"></i>');  
    jQuery(document).ajaxComplete(function() {  
      jQuery('.delete-icon .confirm-ajax-delete').html('<i class="fas fa-trash-alt"></i>');
    });
    
    jQuery(".left-mobile-menu").click(function(){
    jQuery(".menu-list").toggle();	
    });
    
    jQuery('.simplelogin-bg #edit-field-agency').hide();
      jQuery('.user-register-form p.district-markup').hide();
      jQuery('.user-register-form .field--name-field-districts').hide();
      jQuery(".simplelogin-bg #edit-field-roles").change(function(){
        if ( jQuery(this).val() == 'school_admin' || jQuery(this).val() == 'esc_admin') {
          jQuery('.simplelogin-bg #edit-field-agency').show();
          jQuery('.user-register-form p.district-markup').show();
          jQuery('.user-register-form .field--name-field-districts').show();
        } else if(jQuery(this).val() == 'region_18_admin') {
          jQuery('.simplelogin-bg #edit-field-agency').hide();
          jQuery('.user-register-form p.district-markup').show();
          jQuery('.user-register-form .field--name-field-districts').show();
        } else {
          jQuery('.simplelogin-bg #edit-field-agency').hide();
          jQuery('.user-register-form p.district-markup').hide();
          jQuery('.user-register-form .field--name-field-districts').hide();
        }
      });
    
      // Reset Button
      jQuery('.user-register-form input.button--reset').on('click', function() {
        jQuery('.field--name-field-districts input.form-autocomplete').val('');
      });
    
      jQuery(document).ajaxComplete(function() {
        jQuery("body").find('.form-autocomplete').change(function() {
        var val = jQuery(this).val();
        var match = val.match(/\((.*?)\)$/);
        if (match) {
            jQuery(this).data('real-value', val);
            jQuery(this).val(val.replace(' ' + match[0], ''));
        }
    });
      });
    